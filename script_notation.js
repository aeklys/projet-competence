//document.onload=critere();
//var titre_eval = document.getElementById("titre_eval").value;

function ajouter() {

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer "+ document.cookie);
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
        "nom_eval": `${document.getElementById("titre_eval").value}`
      });

    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
    };

    fetch("http://127.0.0.1:8055/items/evaluations", requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log('error', error));
}

/*function supp_item() {

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer "+ document.cookie);

    var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
    };

    fetch("http://127.0.0.1:8055/items/evaluations", requestOptions)
    .then(response => response.json())
    .then(result => {
            console.log(result);
    })
    .catch(error => console.log('error', error));
}*/

function supprimer(){
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer "+ document.cookie);

    var requestOptions = {
    method: 'DELETE',
    headers: myHeaders,
    redirect: 'follow'
};

fetch("http://127.0.0.1:8055/items/evaluations/102", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
}

var l = 0;

function addTable() {
    var Table = document.getElementById("create-table");
    var ligne = document.createElement("tr");
    var td1 = document.createElement("td");

    var textTD1 = document.createElement("input");
    textTD1.setAttribute("type", "text");
    textTD1.setAttribute("class", "create-text-eval");
    textTD1.setAttribute("id", "titre_eval" +(l+1));

    Table.appendChild(ligne);
    ligne.appendChild(td1);
    td1.appendChild(textTD1);

}

function delTable() {
    document.getElementById("create-table").deleteRow(-1);
  }

  //Y9JQIvXxtAYJ-8XrjPvjwRtWRPvv8tJj